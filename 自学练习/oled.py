from machine import Pin, I2C
import ssd1306
import gfx
import dht
from time import sleep

# ESP32 Pin assignment 
#i2c = I2C(-1, scl=Pin(22), sda=Pin(21))

# ESP8266 Pin assignment
i2c = I2C(-1, scl=Pin(5), sda=Pin(4))

oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)
graphics = gfx.GFX(oled_width, oled_height, oled.pixel)


sensor = dht.DHT11(Pin(2))
while True:
  try:
    oled.fill(0)                                    # Screen clean up
    sleep(2)                                        # Waiting sensor init
    sensor.measure()
    temp = sensor.temperature()
    hum = sensor.humidity()
    
    # draw temperature
    oled.text('Temp.:', 0, 1)
    graphics.rect(0, 21, 60, 10, 1)                 # draw bar frame
    graphics.fill_rect(0, 21, temp/100*60, 10, 1)   # fill bar inside of frame
    oled.text('{:0.1f} C'.format(temp), 0, 41)

    # draw humidity
    oled.text('Hum.:', 65, 1)
    graphics.rect(65, 21, 60, 10, 1)
    graphics.fill_rect(65, 21, hum/100*60, 10, 1)    
    oled.text('{:0.1f} %'.format(hum), 65, 41)
    oled.show()                                     # show on screen
  except OSError as e:
    print('Failed to read sensor.')
