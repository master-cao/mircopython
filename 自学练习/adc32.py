from machine import Pin, ADC
from time import sleep

# There are several pins on the ESP32 that can act as analog pins
# these are called ADC pins. All the following GPIOs can act as ADC pins:
# 0, 2, 4, 12, 13, 14, 15, 25, 26, 27, 32, 33, 34, 35, 36, and 39.

from machine import Pin, ADC
from time import sleep

pot = ADC(Pin(34))
pot.atten(ADC.ATTN_11DB)       # Full range: 3.3v

# ADC.ATTN_0DB — the full range voltage: 1.2V
# ADC.ATTN_2_5DB — the full range voltage: 1.5V
# ADC.ATTN_6DB — the full range voltage: 2.0V
# ADC.ATTN_11DB — the full range voltage: 3.3V

# You may want to get values in other ranges.
# You can change the resolution using the width() method as follows:
# ADC.width(bit)

# ADC.WIDTH_9BIT: range 0 to 511
# ADC.WIDTH_10BIT: range 0 to 1023
# ADC.WIDTH_11BIT: range 0 to 2047
# ADC.WIDTH_12BIT: range 0 to 4095

while True:
  pot_value = pot.read()
  print(pot_value)
  sleep(0.1)