from machine import Pin, PWM
from time import sleep

motion = False

def callback(pin):
    global motion
    motion = True
    global interrupt_pin
    interrupt_pin = pin

led = PWM(Pin(14), 5000)
tch = Pin(12, Pin.IN)

tch.irq(trigger = Pin.IRQ_RISING, handler = callback)

while True:
    if motion:
        print('Motion detected,interrupt by:', interrupt_pin)
        for duty_cycle in range(0, 1024):
            led.duty(duty_cycle)
            sleep(0.005)
        sleep(2)
        for duty_cycle in range(0, 1024):
            led.duty(1024-duty_cycle)
            sleep(0.005)
        print('Motion stopped.')
        motion = False
