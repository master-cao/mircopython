from time import sleep

pressed = False

def handle_interrupt(pin):
  global pressed
  pressed = True
  global interrupt_pin
  interrupt_pin = pin 

led = Pin(12, Pin.OUT)
btn = Pin(14, Pin.IN)

btn.irq(trigger=Pin.IRQ_FALLING, handler=handle_interrupt)

while True:
  if pressed:
    print('Motion detected! Interrupt caused by:', interrupt_pin)
    led.value(1)
    sleep(20)
    led.value(0)
    print('Motion stopped!')
    pressed = False