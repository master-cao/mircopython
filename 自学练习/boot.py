# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
#uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
#import webrepl
#webrepl.start()
gc.collect()

try:
  import usocket as socket
except:
  import socket

import network

wlan = network.WLAN(network.STA_IF)
if not wlan.isconnected():                   # Check for successful connection
    print('Connecting to network...')
    wlan.active(True)
    wlan.connect('CMCC-nCzW', 'x43wng6y')    # Connect to an AP
    while not wlan.isconnected():
        pass
print('network config:', wlan.ifconfig())

ap = network.WLAN(network.AP_IF)             # create access-point interface
if not ap.isconnected():                     # activate the AP interface
#    print('Setting up AP...')
    ap.active(True)
    ap.config(essid = 'ESP32-AP', password='iloveyou') # set the ESSID of the access point
#    while not ap.isconnected():
#        pass
print('AP config:',ap.ifconfig())


#socket(AF_INET, SOCK_STREAM)             # Create STREAM TCP socket
#socket(AF_INET, SOCK_DGRAM)              # Create DGRAM UDP socket