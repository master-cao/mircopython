try:
  import usocket as socket
except:
  import socket
  
from machine import Pin
led1 = Pin(14, Pin.OUT) #    Red LED
led2 = Pin(12, Pin.OUT) # Yellow LED
led3 = Pin(13, Pin.OUT) #   Blue LED

def web_page():
  if led1.value() == 1:
    led1_state="ON"
  else:
    led1_state="OFF"
    
  if led2.value() == 1:
    led2_state="ON"
  else:
    led2_state="OFF"
    
  if led3.value() == 1:
    led3_state="ON"
  else:
    led3_state="OFF"
  html = """<html><head><title>ESP Web Server</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>html{font-family: Helvetica; display:inline-block; margin: 5px auto; text-align: center;}
h1{color: #0F3376; padding: 2px;}p{font-size: 1.2rem;}
.button{display: inline-block; background-color: #ff0000; border: none; border-radius: 5px; color: white; padding: 5px 10px; font-size: 16px; margin: 5px; cursor: pointer;}
.button2{background-color: #ffdd00;}
.button3{background-color: #00ff00;}
</style></head>
<body><h1>Home Automation</h1> 
<table><tr><td><p>Led 1: <b>""" + led1_state + """</b></p></td>
<td><p><a href="/?led1=on"><button class="button">LED1 ON</button></a></p></td>
<td><p><a href="/?led1=off"><button class="button">LED1 OFF</button></a></p></td></tr>
<tr><td><p>Led 2: <b>""" + led2_state + """</b></p></td>
<td><p><a href="/?led2=on"><button class="button button2">LED2 ON</button></a></p></td>
<td><p><a href="/?led2=off"><button class="button button2">LED2 OFF</button></a></p></td></tr>
<table><tr><td><p>Led 3: <b>""" + led3_state + """</b></p></td>
<td><p><a href="/?led3=on"><button class="button button3">LED3 ON</button></a></p></td>
<td><p><a href="/?led3=off"><button class="button button3">LED3 OFF</button></a></p></td></tr>
</table></body></html>"""
  return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
  conn, addr = s.accept()
  print('Got a connection from %s' % str(addr))
  request = conn.recv(1024)
  request = str(request)
  print('Content = %s' % request)
  
  led1_on = request.find('/?led1=on')
  led1_off = request.find('/?led1=off')
  
  led2_on = request.find('/?led2=on')
  led2_off = request.find('/?led2=off')
  
  led3_on = request.find('/?led3=on')
  led3_off = request.find('/?led3=off')
  
  if led1_on == 6:
    print('LED1 ON')
    led1.value(1)
  if led1_off == 6:
    print('LED1 OFF')
    led1.value(0)
    
  if led2_on == 6:
    print('LED2 ON')
    led2.value(1)
  if led2_off == 6:
    print('LED2 OFF')
    led2.value(0)
    
  if led3_on == 6:
    print('LED3 ON')
    led3.value(1)
  if led3_off == 6:
    print('LED3 OFF')
    led3.value(0)
    
  response = web_page()
  conn.send('HTTP/1.1 200 OK\n')
  conn.send('Content-Type: text/html\n')
  conn.send('Connection: close\n\n')
  conn.sendall(response)
  conn.close()