from machine import Pin, SPI

# construct an SPI bus on the given pins
# polarity is the idle state of SCK
# phase=0 means sample on the first edge of SCK, phase=1 means the second

# Software SPI bus
spi = SPI(-1, baudrate=100000, polarity=1, phase=0, sck=Pin(0), mosi=Pin(2), miso=Pin(4))

# Hardware SPI bus  (up to 80Mhz, MISO:12, MOSI:13, SCK:14)
#spi = SPI(1, baudrate=80000000, polarity=0, phase=0)

spi.init(baudrate=200000)           # set the baudrate

spi.read(10)                        # read 10 bytes on MISO
spi.read(10, 0xff)                  # read 10 bytes while outputting 0xff on MOSI

buf = bytearray(50)                 # create a buffer
spi.readinto(buf)                   # read into the given buffer (reads 50 bytes in this case)
spi.readinto(buf, 0xff)             # read into the given buffer and output 0xff on MOSI

spi.write(b'12345')                 # write 5 bytes on MOSI

buf = bytearray(4)                  # create a buffer
spi.write_readinto(b'1234', buf)    # write to MOSI and read from MISO into the buffer
spi.write_readinto(buf, buf)        # write buf to MOSI and read MISO back into buf